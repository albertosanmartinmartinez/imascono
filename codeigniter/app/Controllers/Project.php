
<?php

namespace App\Controllers;

class Project extends BaseController {

  public function list() {
    echo "list function";

    return view('project_list');
	}

  public function detail() {

    return view('project_detail');
	}

  public function create() {

    return view('project_create');
	}

  public function update() {

    return view('project_update');
	}

  public function delete() {

    return view('project_delete');
	}
}
