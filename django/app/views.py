
#!/usr/local/bin/python
# -*- coding: utf-8 -*-

import json

from django.views import View
from django.template import loader, RequestContext
from django.shortcuts import render
from django.views.generic.base import TemplateView
from django.template.response import TemplateResponse
from django.views.generic import ListView, DetailView
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.http import HttpResponse, Http404, HttpResponseRedirect, JsonResponse
from django.core import serializers

from rest_framework.decorators import api_view, permission_classes, renderer_classes
from rest_framework.renderers import JSONRenderer, TemplateHTMLRenderer
from rest_framework import permissions
from rest_framework import status
from rest_framework.response import Response

#from app.tokens import account_activation_token
from app import forms as app_forms
from app import models as app_models

from project.settings import base as app_settings


# Create your views here.
