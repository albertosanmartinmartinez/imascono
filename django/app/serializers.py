
#!/usr/local/bin/python
# -*- coding: utf-8 -*-

from rest_framework import serializers

from app import models as app_models


# Create you serializers here
class CompanySerializer(serializers.ModelSerializer):
    """
    """

    class Meta:
        model = app_models.Company
        fields = '__all__'
