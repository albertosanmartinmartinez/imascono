
#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.db import models
from django.utils import timezone
from django.contrib.auth import get_user_model

# Create your models here.
class Project(models.Model):

    created_date = models.DateTimeField(verbose_name='Created date')
    updated_date = models.DateTimeField(verbose_name='Updated date', blank=True, default=timezone.now)

    name = models.CharField(verbose_name='Name', max_length=200)
    description = models.TextField(verbose_name='Description')

    # video
    # image

    class Meta:
        verbose_name = "Project"
        verbose_name_plural = "Projects"
        ordering = ['-created_date']

    def __str__(self):

        return self.name

    def save(self, *args, **kwargs):

        company_date = timezone.localtime()
        self.updated_date = company_date

        if not self.pk:
            self.created_date = company_date

        super(Project, self).save(*args, **kwargs)




#
