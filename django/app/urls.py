
#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import get_user_model
from django.views.generic import RedirectView

from app import urls as app_urls
from app import views as app_views
from app import models as app_models

#from app.folder_views import company_views


# Create your urls here.
urlpatterns = [

    # GENERAL
]
