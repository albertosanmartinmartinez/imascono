
#!/usr/local/bin/python
# -*- coding: utf-8 -*-

from django.contrib import admin
from django.contrib.auth.models import Group, Permission

from app import models as app_models


# Register your models here.
class ProjectAdmin(admin.ModelAdmin):

    list_display = ('id', 'name', 'description')
    #search_fields = ('codename', 'name')
    #list_editable = ('codename', 'name')

admin.site.register(app_models.Project, ProjectAdmin)




#
