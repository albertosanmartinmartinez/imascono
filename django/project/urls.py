
#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.contrib import admin
from django.urls import path, include, re_path
from django.conf.urls.static import static
from django.views.generic import RedirectView
from django.views.static import serve

from django.conf.urls import handler403, handler404, handler500

from project.settings import base as app_settings
from app import urls as app_urls
from app import views as app_views


admin.site.site_header = 'Imascono Django'
admin.autodiscover()

urlpatterns = [

    # Admin Urls
    path('admin/', admin.site.urls),

    #Errors
    #path('errors/403', app_views.error_403, name='error_403'),
    #path('errors/404', app_views.error_404, name='error_404'),
    #path('errors/500', app_views.error_500, name='error_500'),
]

if app_settings.FILES == 'LOCAL':
    urlpatterns += static(app_settings.STATIC_URL, document_root=app_settings.STATIC_ROOT)
    urlpatterns += static(app_settings.MEDIA_URL, document_root=app_settings.MEDIA_ROOT)




#
