
#!/usr/bin/env python
# -*- coding: utf-8 -*-


import environ

env = environ.Env()
environ.Env.read_env()


# ********** Environment Vars **********

SECRET_KEY = env.str('SECRET_KEY')
#print('secret key: ' + str(SECRET_KEY))

DEBUG = env.bool('DEBUG')
#print('debug: ' + str(DEBUG))

APP_DOMAIN = env.str('DOMAIN')
#print('domain: ' + APP_DOMAIN)

ERRORS = env.bool('ERRORS')
#print('errors: ' + ERRORS)




#
