
#!/usr/bin/env python
# -*- coding: utf-8 -*-


import os


# ********** Mail **********

EMAIL_USE_TLS = True
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = os.environ.get('EMAIL_HOST')
#print(EMAIL_HOST)
EMAIL_HOST_USER = os.environ.get('EMAIL_USER')
#print(EMAIL_HOST_USER)
EMAIL_HOST_PASSWORD = os.environ.get('EMAIL_PASSWORD')
#print(EMAIL_HOST_PASSWORD)
EMAIL_PORT = 587




#
