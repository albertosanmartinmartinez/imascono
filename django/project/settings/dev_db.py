
#!/usr/bin/env python
# -*- coding: utf-8 -*-


import environ

env = environ.Env()
environ.Env.read_env()


# ********** Database **********

DB_NAME = env.str('DB_NAME')
DB_USER = env.str('DB_USER')
DB_PASSWORD = env.str('DB_PASSWORD')
DB_HOST = env.str('DB_HOST')
DB_PORT = int(env.str('DB_PORT'))




#
