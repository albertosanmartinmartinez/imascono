
#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import environ


# ********** Paths **********

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
#print(BASE_DIR)

PROJECT_DIR = os.path.join(BASE_DIR, "project")
#print(PROJECT_DIR)

LOG_DIR  = os.path.join(BASE_DIR, "logs")
#print(LOG_DIR)

FIXTURE_DIR  = os.path.join(BASE_DIR, "fixtures")
#print(FIXTURE_DIR)

TEMPLATE_DIR  = os.path.join(BASE_DIR, "templates")
#print(TEMPLATE_DIR)

LOCALE_DIR  = os.path.join(BASE_DIR, "locale")
#print(LOCALE_DIR)


env = environ.Env()
environ.Env.read_env()
#environ.Env.read_env(BASE_DIR + '/' + '.env_base')

# ********** Environment Vars **********

STATUS = 'DEV'
#STATUS = 'PROD'
#print('status: ' + STATUS)

if STATUS == 'DEV':
    from project.settings.dev import *

elif  STATUS == 'PROD':
    from project.settings.prod import *


DB_DEPLOY = 'LOCAL'
#DB_DEPLOY = 'CLOUD'
#print('db : ' + DB_DEPLOY)

if DB_DEPLOY == 'LOCAL':
    from project.settings.dev_db import *

elif  DB_DEPLOY == 'CLOUD':
    from project.settings.prod_db import *

EMAILS = True
#EMAILS = False
#print('emails: ' + str(EMAILS))

if EMAILS:
    from project.settings.dev_mail import *

else:
    from project.settings.prod_mail import *


# ********** Django Config **********

ALLOWED_HOSTS = [
    '*'
]

FIXTURE_DIRS = [
    FIXTURE_DIR
]

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'multiselectfield',
    'ckeditor',
    'widget_tweaks',
    'django_model_changes',
    'storages',

    'app.apps.ImasconoAppConfig',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

SITE_ID = 1

#AUTH_USER_MODEL = 'app.User'

#LOGIN_URL = 'app:custom_login'
#LOGOUT_REDIRECT_URL = 'app:custom_login'


# ********** Urls **********

ROOT_URLCONF = 'project.urls'


# ********** Templates **********

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            TEMPLATE_DIR,
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

# ********** Server **********

WSGI_APPLICATION = 'project.wsgi.application'


# ********** Backends **********

AUTHENTICATION_BACKENDS = [
    #'app.backend.CustomBackend',
    'django.contrib.auth.backends.ModelBackend',
]


# ********** Database **********

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': DB_NAME,
        'USER': DB_USER,
        'PASSWORD': DB_PASSWORD,
        'HOST': DB_HOST,
        'PORT': DB_PORT,
    },
}

# ********** Password Validators **********

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
    #{'NAME': 'app.validators.SpecialCharactersValidator'}
]


# ********** Internationalization **********

LANGUAGE_CODE = 'en'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


TIME_INPUT_FORMATS = [
    #'%H:%M:%S',     # '14:30:59'
    '%H:%M',        # '14:30'
    '%I:%M %p'
]

# ********** Files **********

#FILE_OVERWRITE = True
FILE_OVERWRITE = False

#FILE_UPLOAD_MAX_MEMORY_SIZE = 0

FILES = 'LOCAL'
#FILES = 'CLOUD'
#print('files: ' + FILES)

if FILES == 'LOCAL':
    from project.settings.dev_files import *

elif  FILES == 'CLOUD':
    from project.settings.prod_files import *


# ********** Static Files **********

STATICFILES_FINDERS = [
   "django.contrib.staticfiles.finders.FileSystemFinder",
   "django.contrib.staticfiles.finders.AppDirectoriesFinder"
]

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static"),
]

STATIC_ROOT = os.path.join(BASE_DIR, "staticfiles")


# ********** Media Files **********

MEDIAFILES_DIRS = [
    os.path.join(BASE_DIR, "media"),
]

MEDIA_ROOT = os.path.join(BASE_DIR, "media")

FILE_UPLOAD_MAX_MEMORY_SIZE = 10485760

# ********** CKEDITOR CONFIG **********

CKEDITOR_UPLOAD_PATH = 'ckeditor/'
CKEDITOR_UPLOAD_SLUGIFY_FILENAME = False
CKEDITOR_JQUERY_URL = 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js'
CKEDITOR_IMAGE_BACKEND = 'pillow'

CKEDITOR_CONFIGS = {
    'full': {
        'extraPlugins': ','.join([
            'autogrow',
            'resize'
        ]),
        'resize_enabled': True,
        'toolbar': 'Full',
        'width': '100%',
        #'resize_minWidth': '20%',
        #'resize_maxWidth': '100%',
        #'resize_minHeight': 300,
        #'resize_maxHeight': 600,
        #'resize_dir': 'both',
        #'autoGrow_minHeight': 300,
        #'autoGrow_maxHeight': 600,
        'allowedContent': True,
        'enterMode': 2,
    },
    'basic': {
        'extraPlugins': ','.join([
            'autogrow',
            'resize'
        ]),
        'resize_enabled': True,
        'toolbar': 'Basic',
        'width': '100%',
        #'resize_minWidth': '20%',
        #'resize_maxWidth': '100%',
        #'resize_minHeight': 300,
        #'resize_maxHeight': 600,
        #'resize_dir': 'both',
        #'autoGrow_minHeight': 300,
        #'autoGrow_maxHeight': 600,
        'allowedContent': True,
        'enterMode': 2,
    },
}


# ********** Logging **********

# ********** Errors **********

# ********** Caching **********

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': '/var/tmp/django_cache',
    }
}




#
